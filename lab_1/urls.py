from django.urls import re_path
from .views import index, index2, index3, index4, portfolio_form, portfolio_form_output
#url for app
urlpatterns = [
    re_path(r'^$', index, name='index'),
    re_path(r'^aboutme/', index2, name='index2'),
    re_path(r'^resume/', index3, name='index3'),
    re_path(r'^contacts/', index4, name='index4'),
    re_path(r'^portfolioform/', portfolio_form, name='portfolio_form'),
    re_path(r'^portfolio/', portfolio_form_output, name='portfolio_form_output'),
]
