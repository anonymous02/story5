from django.db import models

# Create your models here.

class Portfolio(models.Model):
    judul = models.CharField(max_length=120)
    tgl_mulai = models.DateField()
    tgl_selesai = models.DateField()
    deskripsi = models.TextField(blank=True, null = True)
    tempat = models.CharField(max_length=120)
    kategori = models.CharField(max_length=120)
