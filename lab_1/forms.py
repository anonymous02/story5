from django import forms

from .models import Portfolio

class PortfolioForm(forms.ModelForm):
    class Meta:
        model = Portfolio
        fields = [
            'judul',
            'tgl_mulai',
            'tgl_selesai',
            'deskripsi',
            'tempat',
            'kategori'
        ]