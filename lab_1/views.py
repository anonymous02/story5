from django.shortcuts import render
from .forms import PortfolioForm
from .models import Portfolio
import datetime

date = {'date' : datetime.datetime.now()}

def index(request):
    return render(request, 'index.html', date)

def index2(request):
    return render(request, 'index2.html', date)

def index3(request):
    return render(request, 'index3.html', date)

def index4(request):
    return render(request, 'index4.html', date)

def portfolio_form(request):
    form = PortfolioForm(request.POST or None)
    if (form.is_valid() and request.method == 'POST'):
        form.save()
        form = PortfolioForm()
    context = {
        'portfolio_form' : form
    }
    return render(request, "portfolio_form.html", context)

def portfolio_form_output(request):
    form_output = Portfolio.objects.all()
    context = {
        'portfolio_form_output' : form_output
    }
    return render(request, "portfolio_form_output.html", context)
